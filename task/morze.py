SUPPLEMENTARY={'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.', 'F': '..-.', 'G': '--.', 'H': '....', 'I': '..', 'J': '.---', 'K': '-.-', 'L': '.-..', 'M': '--', 'N': '-.', 'O': '---', 'P': '.--.', 'Q': '--.-', 'R': '.-.', 'S': '...', 'T': '-', 'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-', 'Y': '-.--', 'Z': '--..',
'1': '.----', '2': '..---', '3': '...--',
'4': '....-', '5': '.....', '6': '-....',
'7': '--...', '8': '---..', '9': '----.', '0': '-----', ', ': '--..--', '.': '.-.-.-', '?': '..--..', '/': '-..-.', '-': '-....-', '(': '-.--.', ')': '-.--.-'}

def code_morze(value):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''

    text=text.upper()
    morze_code=[]

    for char in text:
        if char!=' ':
            morze_char=SUPPLEMENTARY.get(char,char)
            morze_code.append(morze_char)
    return ' '.join(morze_code)

input_text = input("Type the text: ")
encoded_text = code_morze(input_text)
print("Morse code: ", encoded_text)
